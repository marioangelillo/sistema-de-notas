class Nota{
    constructor(id, titulo, contenido, fecha, categoria){
        this.id = id;
        this.titulo = titulo;
        this.contenido = contenido;
        this.fecha = fecha
        this.categoria = categoria
    }
}

class Categorias{
    constructor(nombre) {
        this.nombre = nombre;
    }
}

let existe = false;
let notas = JSON.parse(localStorage.getItem('notas')) || [];
let categorias = JSON.parse(localStorage.getItem('categorias')) || [];

mostrarNotas(notas);
mostrarCategorias();

function actualizarHora(){
    let fecha = new Date();
    let dia = fecha.getDate();
    let mes = fecha.getMonth();
    let meses = ['Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo', 'Junio', 'Julio', 'Agosto', 'Septiembre', 'Octubre', 'Noviembre', 'Diciembre'];
    let ano = fecha.getFullYear();
    let hora = fecha.getHours();
    let minutos = fecha.getMinutes();
    let segundos = fecha.getSeconds();

    return dia +' '+ meses[mes]+ ' ' + ano + ' ' + hora + ':' + minutos + ':' + segundos;
}

function generarId(){
    var id = '';
    var caracteres = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';
    var caracteresLength = caracteres.length;
    for (var i = 0; i < 20; i++) {
        id += caracteres.charAt(Math.floor(Math.random() * caracteresLength));
    }
    return id;
}

function mostrarNotas(notas){
    let tabla = document.getElementById('tabla-notas');
    tabla.innerHTML = '';
    let contenido = '';

    for (let i = 0; i < notas.length; i++) {
        contenido +='<tr>';
        contenido += '<td>'+notas[i].id+'</td>';
        contenido += '<td>'+notas[i].titulo+'</td>';
        contenido += '<td>'+notas[i].fecha+' </td>'
        contenido += '<td><button type="button" class="btn btn-primary pl-4 pr-4">Ver</button></td>';
        contenido +='</tr>';
    }
    tabla.innerHTML = contenido;
}

function guardarNota(){
    if(valorSelect() == 'Seleccione una categoria'){
        alert('Debe seleccionar una categoria');
        return;
    }
    let titulo = document.getElementById('titulo-nota');
    let contenido = document.getElementById('contenido-nota');

    if(titulo.value){
        existeNota();
        if(existe){
            if(confirm('Ya existe una nota con ese titulo, ¿Desea modificarla?')){
                for (let i = 0; i < notas.length; i++) {
                    for (let i = 0; i < notas.length; i++) {
                        if(notas[i].titulo.toUpperCase() == document.getElementById('titulo-nota').value.toUpperCase()){
                            notas.splice(i, 1); 
                        }
                    }
                }
                let nota = new Nota(generarId(), titulo.value, contenido.value, actualizarHora(), valorSelect())
                notas.push(nota);
                localStorage.setItem('notas', JSON.stringify(notas));
            }
            existe = false;
        }else{
            let nota = new Nota(generarId(), titulo.value, contenido.value, actualizarHora(), valorSelect())
            notas.push(nota);
            localStorage.setItem('notas', JSON.stringify(notas));
        }
    }else{
        alert('Debe ingresar el título de la nota');
    }
    limpiarNota();
    mostrarNotas(notas);
}

function mostrarNotas(notas){
    let tabla = document.getElementById('tabla-notas');
    tabla.innerHTML = '';
    let contenido = '';

    for (let i = 0; i < notas.length; i++) {
        contenido +='<tr class = "bordes">';
        contenido += '<td >'+notas[i].id+'</td>';
        contenido += '<td>'+notas[i].titulo+'</td>';
        contenido += '<td>'+notas[i].fecha+' </td>'
        contenido += '<td>'+notas[i].categoria+' </td>'
        contenido += '<td><button type="button" class="btn btn-primary pl-4 pr-4" onclick = "verNota(\''+notas[i].titulo+'\')">Ver</button></td>';
        contenido +='</tr>';
    }
    tabla.innerHTML = contenido;
}

function mostrarCategorias(){
    let opciones = document.getElementById('categorias');
    opciones.innerHTML = '';
    let contenido = '';
    contenido += '<option>Seleccione una categoria</option>';

    for (let i = 0; i < categorias.length; i++) {
        contenido += '<option>'+categorias[i].nombre+'</option>'
    }
    opciones.innerHTML = contenido;
}

function existeNota(){
    let titulo = document.getElementById('titulo-nota').value;
    for (let i = 0; i < notas.length; i++) {
        if(titulo.toUpperCase() == notas[i].titulo.toUpperCase()){
            existe = true;
        }
    }
}
function verNota(titulo){
    document.getElementById('titulo-nota').value = titulo;
    for (let i = 0; i < notas.length; i++) {
        if(notas[i].titulo == titulo){
            document.getElementById('contenido-nota').value = notas[i].contenido;
            document.getElementById('categorias').value = notas[i].categoria;
        }   
    }
}

function limpiarNota(){
    document.getElementById('titulo-nota').value = '';
    document.getElementById('contenido-nota').value = '';
    document.getElementById('categorias').value = 'Seleccione una categoria';
    
}

function borrarNota(){
    for (let i = 0; i < notas.length; i++) {
        if(notas[i].titulo.toUpperCase() == document.getElementById('titulo-nota').value.toUpperCase()){
            if(confirm('¿Esta seguro que desea eliminar?')){
                notas.splice(i, 1);
            }
        }
    }
    localStorage.setItem('notas', JSON.stringify(notas));
    mostrarNotas(notas);
    limpiarNota(); 

}

function buscarNota(){
    let buscar = document.getElementById('buscar').value.toUpperCase();
    let notasFiltrada = notas.filter(nota => nota.titulo.toUpperCase().includes(buscar) || nota.contenido.toUpperCase().includes(buscar) || nota.categoria.toUpperCase().includes(buscar));
    mostrarNotas(notasFiltrada);
}

function agregarCategoria(){
    let nombrecategoria = document.getElementById('agregar-categoria');
    if(nombrecategoria.value.length <= 30){
        let categoria = new Categorias(nombrecategoria.value);
        categorias.push(categoria);
        localStorage.setItem('categorias', JSON.stringify(categorias));
        nombrecategoria.value = '';
        alert('Categoria agregada correctamente');
        mostrarCategorias();
    }else{
        alert('Máximo 30 caracteres para el nombre de la categoría');
    }
}

function valorSelect(){
    let e = document.getElementById('categorias');
    //let value = e.selectElement.options[e.selectedIndex].value;// get selected option value
    var text=e.options[e.selectedIndex].text;//get the selected option text
    return text;
}

function borrarCategoria(){
    let categoria = document.getElementById('agregar-categoria');
    for (let i = 0; i < notas.length; i++) {
        if(notas[i].categoria.toUpperCase() == categoria.value.toUpperCase()){
            alert('Existen notas con esta categoría');
        }else{
            for (let i = 0; i < categorias.length; i++) {
                if(categorias[i].nombre.toUpperCase() == categoria.value.toUpperCase()){
                    categorias.splice(i, 1);
                    alert('Categoria eliminada correctamente')
                }
            }
        }
    }
    localStorage.setItem('categorias', JSON.stringify(categorias));
    document.getElementById('agregar-categoria').value = '';
    mostrarCategorias();
}